import axios from 'axios'
import { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'

const URI='http://localhost:9000/api/medicos/'
const CompMostrarMedico=()=>{
    const [medicos,setMedico]= useState([])
    useEffect(() =>{
        getMedicos()
    },[])

//vamos a mostrar todas las citas
const getMedicos= async()=>{
    const res=await axios.get(URI)
    setMedico(res.data);
}
//eliminar citas
const deleteMedicos= async(id)=>{
    await axios.delete(` ${URI}${id}`)
    getMedicos()

}

return(
    <div className='container'>
    <div className='row'>
    <div className='column'>
    <Link to="/medicos/crear" className='btn btn-info mt-2 mb-2'><i className="fa-solid fa-plus"></i><i class="fa-solid fa-user-nurse"></i></Link>
    <table className='table'>
        <thead className='tableTheadBg'>
        <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Cedula</th>
            <th>Telefono</th>
            <th>Email </th>
            <th>Fecha_Nacimiento</th>
            <th>Cargo </th>
            <th>Direccion</th>
            <th>Numero_Consultorio</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            {medicos.map  ((medico,index) =>(
                <tr key ={index}>
                    <td>{medico.nombres}</td>
                    <td>{medico.apellidos}</td>
                    <td>{medico.cedula}</td>
                    <td>{medico.telefono}</td>
                    <td>{medico.email}</td>
                    <td>{medico.fecha_nacimiento}</td>
                    <td>{medico.cargo}</td>
                    <td>{medico.direccion}</td>
                    <td>{medico.numero_consultorio}</td>
                   
                    <td>
                    <Link to={`/medicos/editar/${medico._id}`} className= 'btn btn-success mt-2 mb-2 '><i class="fa-solid fa-pen-to-square"></i></Link>

                        


                        
                        
                        <button onClick={()=>deleteMedicos(medico._id)} className= 'btn btn-danger mt-2 mb-2'><i className="fa-solid fa-trash-can"></i></button>
                    </td>
                    
                </tr>


            ))}
        </tbody>




    </table>


    </div>
    </div>
    </div>


)


}
export default CompMostrarMedico;