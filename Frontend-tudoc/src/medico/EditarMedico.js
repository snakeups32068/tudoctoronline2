import axios from 'axios'
import { useState, useEffect} from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const URI='http://localhost:9000/api/medicos/'

const CompEditarMedicos=()=>{
    const[nombres,setNombres]=useState('');
    const[apellidos,setApellidos]=useState('');
    const[cedula,setCedula]=useState('');
    const[telefono,setTelefono]=useState('');
    const[email,setEmail]=useState('');
    const[fecha_nacimiento,setFecha_nacimiento]=useState('');
    const[cargo,setCargo]=useState('');
    const[direccion,setDireccion]=useState('');
    const[numero_consultorio,setNumero_consultorio]=useState('');
    const navigate= useNavigate();
    const {id} = useParams();
    //funcion actualizar

const Update= async(g) =>{
    g.preventDefault();
    await axios.put(`${URI}${id}`,{ nombres:nombres, apellidos:apellidos,cedula:cedula,
    telefono:telefono,email:email,fecha_nacimiento:fecha_nacimiento,cargo:cargo,direccion:direccion,numero_consultorio:numero_consultorio});
    navigate('/');
}
useEffect(()=>{
    getmedicoById()
    // eslint-disable-next-line

},[])
const getmedicoById= async()=>{
    const res= await axios.get(`${URI}${id}`)
    setNombres(res.data.nombres);
    setApellidos(res.data.apellidos);
    setCedula(res.data.cedula);
    setTelefono(res.data.telefono);
    setEmail(res.data.email);
    setFecha_nacimiento(res.data.fecha_nacimiento);
    setCargo(res.data.cargo);
    setDireccion(res.data.direccion);
    setNumero_consultorio(res.data.numero_consultorio);
}

return(
    <div>
        <h2>Modulo Editar Medicos</h2>
        <form onSubmit={Update}>

        <div className='mb-3'>
        <label className='form-label'>Nombres</label>
        <input value={nombres} onChange={(g) =>setNombres(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Apellidos</label>
        <input value={apellidos} onChange={(g) =>setApellidos(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Cedula</label>
        <input value={cedula} onChange={(g) =>setCedula(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Telefono</label>
        <input value={telefono} onChange={(g) =>setTelefono(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Email</label>
        <input value={email} onChange={(g) =>setEmail(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Fecha_Nacimiento</label>
        <input value={fecha_nacimiento} onChange={(g) =>setFecha_nacimiento(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Cargo</label>
        <input value={cargo} onChange={(g) =>setCargo(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Direccion</label>
        <input value={direccion} onChange={(g) =>setDireccion(g.target.value)}
        type="text" className='form-control' />
        </div>
        <div className='mb-3'>
        <label className='form-label'> Numero_Consultorio</label>
        <input value={numero_consultorio} onChange={(g) =>setNumero_consultorio(g.target.value)}
        type="number" className='form-control' />
        </div>
        
        <button type='submit' className='btn btn-primary'>Guardar</button>





        </form>
        </div>
    )






}
export default CompEditarMedicos;