import axios from 'axios'
import { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'

const URI='http://localhost:9000/api/pacientes/'
const CompMostrarPaciente=()=>{
    const [pacientes,setPaciente]= useState([])
    useEffect(() =>{
        getPacientes()
    },[])

//vamos a mostrar todas las citas
const getPacientes= async()=>{
    const res=await axios.get(URI)
    setPaciente(res.data);
}
//eliminar citas
const deletePacientes= async(id)=>{
    await axios.delete(` ${URI}${id}`)
    getPacientes()

}

return(
    <div className='container'>
    <div className='row'>
    <div className='column'>
    <Link to="/pacientes/crear" className='btn btn-info mt-2 mb-2'><i className="fa-solid fa-plus"></i><i class="fa-solid fa-user"></i></Link>
    <table className='table'>
        <thead className='tableTheadBg'>
        <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Cedula</th>
            <th>Telefono</th>
            <th>Email </th>
            <th>Fecha_Nacimiento</th>
            <th>Tipo_Sangre </th>
            <th>Direccion</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            {pacientes.map  ((paciente,index) =>(
                <tr key ={index}>
                    <td>{paciente.nombres}</td>
                    <td>{paciente.apellidos}</td>
                    <td>{paciente.cedula}</td>
                    <td>{paciente.telefono}</td>
                    <td>{paciente.email}</td>
                    <td>{paciente.fecha_nacimiento}</td>
                    <td>{paciente.tipo_sangre}</td>
                    <td>{paciente.direccion}</td>
                   
                    <td>
                        <Link to={`/pacientes/editar/${paciente._id}`} className= 'btn btn-success mt-2 mb-2 '><i class="fa-solid fa-pen-to-square"></i></Link>

                        


                        
                        
                        <button onClick={()=>deletePacientes(paciente._id)} className= 'btn btn-danger mt-2 mb-2'><i className="fa-solid fa-trash-can"></i></button>
                    </td>
                    
                </tr>


            ))}
        </tbody>




    </table>


    </div>
    </div>
    </div>


)


}
export default CompMostrarPaciente;