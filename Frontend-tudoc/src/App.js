
import './App.css';
import { useAuth0 } from '@auth0/auth0-react';

import Profile from './Profile';


//importamos componentes
import CompMostrarCita from './cita/MostrarCita';
import CompCrearCitas from './cita/CrearCita';
import CompEditarCitas from './cita/EditarCita';

import CompMostrarPaciente from './paciente/MostrarPaciente';
import CompCrearPacientes from './paciente/CrearPaciente';
import CompEditarPacientes from './paciente/EditarPaciente';

import CompMostrarMedico from './medico/MostrarMedico';
import CompCrearMedicos from './medico/CrearMedico';
import CompEditarMedicos from './medico/EditarMedico';

import CompMostrarMedicamento from './medicamento/MostrarMedicamento';
import CompCrearMedicamentos from './medicamento/CrearMedicamento';
import CompEditarMedicamentos from './medicamento/EditarMedicamento';

import CompMostrarHospitalizado from './hospitalizado/MostrarHospitalizado';
import CompCrearHospitalizados from './hospitalizado/CrearHospitalizado';
import CompEditarHospitalizados from './hospitalizado/EditarHospitalizado';



//importamos rutas
import{ BrowserRouter, Route, Routes} from 'react-router-dom';
import LogoutButton from './Logout';




function App() {
  const{ loginWithRedirect } =useAuth0();
  const { logout } = useAuth0();
  return (
   
    
    <div className="App">
      
      <LogoutButton/>
      <Profile/>
      <div className='BarraA'></div>
      <div className="mensaje"><h2>Para poder Utilizar los modulos es necesario iniciar sesion </h2></div>
      
      
      <header className="App-header"><hr></hr><hr></hr><img src="ine.png" width={180} className="tudoc"></img>
      <button className='btnlogin' onClick={() => loginWithRedirect()}>Iniciar Sesion</button>
      <button className="btnlogout" onClick={() => logout({ returnTo: window.location.origin })}>
      Cerrar Sesion
    </button>
    <a href="javascript: history.go(-1)" class="back"><i class="fa-sharp fa-solid fa-circle-arrow-left fa-3x"></i></a>
    
        </header>
       
        <BrowserRouter>
        <Routes>
        <Route path='/citas' element={<CompMostrarCita />}></Route>
        <Route path='/citas/crear' element={<CompCrearCitas />}></Route>
        <Route path='/citas/editar/:id' element={<CompEditarCitas />}></Route>
        </Routes>
        </BrowserRouter>
        
        <BrowserRouter>
        <Routes>
        <Route path='/pacientes' element={<CompMostrarPaciente />}></Route>
        <Route path='/pacientes/crear' element={<CompCrearPacientes />}></Route>
        <Route path='/pacientes/editar/:id' element={<CompEditarPacientes />}></Route>
        </Routes>
        </BrowserRouter>

        <BrowserRouter>
        <Routes>
        <Route path='/medicos' element={<CompMostrarMedico />}></Route>
        <Route path='/medicos/crear' element={<CompCrearMedicos />}></Route>
        <Route path='/medicos/editar/:id' element={<CompEditarMedicos />}></Route>
        </Routes>
        </BrowserRouter>

        <BrowserRouter>
        <Routes>
        <Route path='/medicamentos' element={<CompMostrarMedicamento/>}></Route>
        <Route path='/medicamentos/crear' element={<CompCrearMedicamentos />}></Route>
        <Route path='/medicamentos/editar/:id' element={<CompEditarMedicamentos />}></Route>
        </Routes>
        </BrowserRouter>

        <BrowserRouter>
        <Routes>
        <Route path='/hospitalizados' element={<CompMostrarHospitalizado/>}></Route>
        <Route path='/hospitalizados/crear' element={<CompCrearHospitalizados />}></Route>
        <Route path='/hospitalizados/editar/:id' element={<CompEditarHospitalizados />}></Route>
        </Routes>
        </BrowserRouter>

        
    </div>
    
    
    
  );
}

export default App;
