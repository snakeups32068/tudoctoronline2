import axios from 'axios'
import { useState} from 'react'
import { useNavigate } from 'react-router-dom'

const URI='http://localhost:9000/api/citas/'

const CompCrearCitas=()=>{
    const[nombres,setNombres]=useState('');
    const[apellidos,setApellidos]=useState('');
    const[cedula,setCedula]=useState('');
    const[especialidad,setEspecialidad]=useState('');
    const[doctor,setDoctor]=useState('');
    const[fecha,setFecha]=useState('');
    const[hora,setHora]=useState('');
    const[numero_consultorio,setNumero_consultorio]=useState('');
    const navigate= useNavigate();

    //funcion guardar
    const GuardarC= async(g) =>{
        g.preventDefault();
        await axios.post(URI,{ nombres:nombres, apellidos:apellidos,cedula:cedula,
        especialidad:especialidad,doctor:doctor,fecha:fecha,hora:hora,numero_consultorio:numero_consultorio});
        navigate('/');

    }
    return(
        <div>
        <h2>Modulo Guardar Citas</h2>
        <form onSubmit={GuardarC}>

        <div className='mb-3'>
        <label className='form-label'>Nombres</label>
        <input value={nombres} onChange={(g) =>setNombres(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Apellidos</label>
        <input value={apellidos} onChange={(g) =>setApellidos(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Cedula</label>
        <input value={cedula} onChange={(g) =>setCedula(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Especialidad</label>
        <input value={especialidad} onChange={(g) =>setEspecialidad(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Doctor</label>
        <input value={doctor} onChange={(g) =>setDoctor(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Fecha</label>
        <input value={fecha} onChange={(g) =>setFecha(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Hora</label>
        <input value={hora} onChange={(g) =>setHora(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Numero_consultorio</label>
        <input value={numero_consultorio} onChange={(g) =>setNumero_consultorio(g.target.value)}
        type="number" className='form-control' />
        </div>
        <button type='submit' className='btn btn-primary'>Guardar</button>





        </form>
        </div>
    )}
export default CompCrearCitas;


