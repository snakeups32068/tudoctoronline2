import axios from 'axios'
import { useState, useEffect} from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const URI='http://localhost:9000/api/citas/'

const CompEditarCitas=()=>{
    const[nombres,setNombres]=useState('');
    const[apellidos,setApellidos]=useState('');
    const[cedula,setCedula]=useState('');
    const[especialidad,setEspecialidad]=useState('');
    const[doctor,setDoctor]=useState('');
    const[fecha,setFecha]=useState('');
    const[hora,setHora]=useState('');
    const[numero_consultorio,setNumero_consultorio]=useState('');
    const navigate= useNavigate();
    const {id} = useParams();

//funcion actualizar

const Update= async(g) =>{
    g.preventDefault();
    await axios.put(`${URI}${id}`,{ nombres:nombres, apellidos:apellidos,cedula:cedula,especialidad:especialidad,doctor:doctor,fecha:fecha,hora:hora,numero_consultorio:numero_consultorio});
    navigate('/');
}
useEffect(()=>{
    getcitaById()
    // eslint-disable-next-line

},[])
const getcitaById= async()=>{
    const res= await axios.get(`${URI}${id}`)
    setNombres(res.data.nombres);
    setApellidos(res.data.apellidos);
    setCedula(res.data.cedula);
    setEspecialidad(res.data.especialidad);
    setDoctor(res.data.doctor);
    setFecha(res.data.fecha);
    setHora(res.data.hora);
    setNumero_consultorio(res.data.numero_consultorio);
}

return(
    <div>
        <h2>Modulo Editar Citas</h2>
        <form onSubmit={Update}>

        <div className='mb-3'>
        <label className='form-label'>Nombres</label>
        <input value={nombres} onChange={(g) =>setNombres(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Apellidos</label>
        <input value={apellidos} onChange={(g) =>setApellidos(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Cedula</label>
        <input value={cedula} onChange={(g) =>setCedula(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Especialidad</label>
        <input value={especialidad} onChange={(g) =>setEspecialidad(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Doctor</label>
        <input value={doctor} onChange={(g) =>setDoctor(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Fecha</label>
        <input value={fecha} onChange={(g) =>setFecha(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Hora</label>
        <input value={hora} onChange={(g) =>setHora(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Numero_Consultorio</label>
        <input value={numero_consultorio} onChange={(g) =>setNumero_consultorio(g.target.value)}
        type="number" className='form-control' />
        </div>
        <button type='submit' className='btn btn-primary'>Guardar</button>





        </form>
        </div>
    )






}
export default CompEditarCitas;