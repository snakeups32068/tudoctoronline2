import axios from 'axios'
import { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'

const URI='http://localhost:9000/api/citas/'
const CompMostrarCita=()=>{
    const [citas,setCita]= useState([])
    useEffect(() =>{
        getCitas()
    },[])

//vamos a mostrar todas las citas
const getCitas= async()=>{
    const res=await axios.get(URI)
    setCita(res.data);
}
//eliminar citas
const deleteCitas= async(id)=>{
    await axios.delete(` ${URI}${id}`)
    getCitas()

}

return(
    <div className='container'>
    <div className='row'>
    <div className='column'>
    <Link to="/citas/crear" className='btn btn-info mt-2 mb-2'><i className="fa-solid fa-plus"></i><i class="fa-solid fa-stethoscope"></i></Link>
    <table className='table'>
        <thead className='tableTheadBg'>
        <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Cedula</th>
            <th>Especialidad</th>
            <th>Doctor </th>
            <th>Fecha</th>
            <th>Hora </th>
            <th>Numero_Consultorio</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            {citas.map  ((cita,index) =>(
                <tr key ={index}>
                    <td>{cita.nombres}</td>
                    <td>{cita.apellidos}</td>
                    <td>{cita.cedula}</td>
                    <td>{cita.especialidad}</td>
                    <td>{cita.doctor}</td>
                    <td>{cita.fecha}</td>
                    <td>{cita.hora}</td>
                    <td>{cita.numero_consultorio}</td>
                   
                    <td>
                        <Link to={`/citas/editar/${cita._id}`} className= 'btn btn-success mt-2 mb-2 '><i class="fa-solid fa-pen-to-square"></i></Link>

                        


                        
                        
                        <button onClick={()=>deleteCitas(cita._id)} className= 'btn btn-danger mt-2 mb-2'><i className="fa-solid fa-trash-can"></i></button>
                    </td>
                    
                </tr>


            ))}
        </tbody>




    </table>


    </div>
    </div>
    </div>


)


}
export default CompMostrarCita;