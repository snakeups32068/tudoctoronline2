import axios from 'axios'
import { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'

const URI='http://localhost:9000/api/medicamentos/'
const CompMostrarMedicamento=()=>{
    const [medicamentos,setMedicamento]= useState([])
    useEffect(() =>{
        getMedicamentos()
    },[])

//vamos a mostrar todas las citas
const getMedicamentos= async()=>{
    const res=await axios.get(URI)
    setMedicamento(res.data);
}
//eliminar citas
const deleteMedicamentos= async(id)=>{
    await axios.delete(` ${URI}${id}`)
    getMedicamentos()

}

return(
    <div className='container'>
    <div className='row'>
    <div className='column'>
    <Link to="/medicamentos/crear" className='btn btn-info mt-2 mb-2'><i className="fa-solid fa-plus"></i><i class="fa-solid fa-capsules"></i></Link>
    <table className='table'>
        <thead className='tableTheadBg'>
        <tr>
            <th>Nombre_Medicamento</th>
            <th>Fecha_Vencimiento</th>
            <th>Cantidad</th>
            <th>Numero_Registro</th>
            <th>Nombre_Laboratorio </th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            {medicamentos.map  ((medicamento,index) =>(
                <tr key ={index}>
                    <td>{medicamento.nombre_medicamento}</td>
                    <td>{medicamento.fecha_vencimiento}</td>
                    <td>{medicamento.cantidad}</td>
                    <td>{medicamento.numero_registro}</td>
                    <td>{medicamento.nombre_laboratorio}</td>
                    <td>
                        <Link to={`/medicamentos/editar/${medicamento._id}`} className= 'btn btn-success mt-2 mb-2 '><i class="fa-solid fa-pen-to-square"></i></Link>

                        


                        
                        
                        <button onClick={()=>deleteMedicamentos(medicamento._id)} className= 'btn btn-danger mt-2 mb-2'><i className="fa-solid fa-trash-can"></i></button>
                    </td>
                    
                </tr>


            ))}
        </tbody>




    </table>


    </div>
    </div>
    </div>


)


}
export default CompMostrarMedicamento;