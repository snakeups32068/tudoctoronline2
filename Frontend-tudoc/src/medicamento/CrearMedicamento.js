import axios from 'axios'
import { useState} from 'react'
import { useNavigate } from 'react-router-dom'

const URI='http://localhost:9000/api/medicamentos/'

const CompCrearMedicamentos=()=>{
    const[nombre_medicamento,setNombre_medicamento]=useState('');
    const[fecha_vencimiento,setFecha_vencimiento]=useState('');
    const[cantidad,setCantidad]=useState('');
    const[numero_registro,setNumero_registro]=useState('');
    const[nombre_laboratorio,setNombre_laboratorio]=useState('');
    const navigate= useNavigate();

    //funcion guardar
    const GuardarC= async(g) =>{
        g.preventDefault();
        await axios.post(URI,{ nombre_medicamento:nombre_medicamento, fecha_vencimiento:fecha_vencimiento,cantidad:cantidad,
        numero_registro:numero_registro,nombre_laboratorio:nombre_laboratorio});
        navigate('/');

    }
    return(
        <div>
        <h2>Modulo Guardar Medicamentos</h2>
        <form onSubmit={GuardarC}>

        <div className='mb-3'>
        <label className='form-label'>Nombre_Medicamento</label>
        <input value={nombre_medicamento} onChange={(g) =>setNombre_medicamento(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Fecha_Vencimiento</label>
        <input value={fecha_vencimiento} onChange={(g) =>setFecha_vencimiento(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Cantidad</label>
        <input value={cantidad} onChange={(g) =>setCantidad(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Numero_Registro</label>
        <input value={numero_registro} onChange={(g) =>setNumero_registro(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Nombre_Laboratorio</label>
        <input value={nombre_laboratorio} onChange={(g) =>setNombre_laboratorio(g.target.value)}
        type="text" className='form-control' />
        </div>
        <button type='submit' className='btn btn-primary'>Guardar</button>





        </form>
        </div>
    )}
export default CompCrearMedicamentos;