import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

const Profile = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();

  if (isLoading) {
    return  ;
  }
  return (
    
    isAuthenticated && (
        
      <div className="perfil">
        <img src={user.picture} alt={user.name} width="80px" className="fotoper"/>
        <p>{user.name }</p>
        <a href="http://localhost:3000/Menu.html" class="nav-nav"> Modulos</a>
        

      </div>
    )
    
  );
};

export default Profile;