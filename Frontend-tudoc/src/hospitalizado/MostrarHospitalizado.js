import axios from 'axios'
import { useState,useEffect } from 'react'
import { Link } from 'react-router-dom'

const URI='http://localhost:9000/api/hospitalizados/'
const CompMostrarHospitalizado=()=>{
    const [hospitalizados,setHospitalizado]= useState([])
    useEffect(() =>{
        getHospitalizados()
    },[])

//vamos a mostrar todas las citas
const getHospitalizados= async()=>{
    const res=await axios.get(URI)
    setHospitalizado(res.data);
}
//eliminar citas
const deleteHospitalizados= async(id)=>{
    await axios.delete(` ${URI}${id}`)
    getHospitalizados()

}

return(
    <div className='container'>
    <div className='row'>
    <div className='column'>
    <Link to="/hospitalizados/crear" className='btn btn-info mt-2 mb-2'><i className="fa-solid fa-plus"></i><i class="fa-solid fa-bed"></i></Link>
    <table className='table'>
        <thead className='tableTheadBg'>
        <tr>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Numero_Cama</th>
            <th>Cedula</th>
            <th>Fecha_Entrada </th>
            <th>Fecha_Salida</th>
            <th>Diagnostico </th>
            
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            {hospitalizados.map  ((hospitalizado,index) =>(
                <tr key ={index}>
                    <td>{hospitalizado.nombres}</td>
                    <td>{hospitalizado.apellidos}</td>
                    <td>{hospitalizado.numero_cama}</td>
                    <td>{hospitalizado.cedula}</td>
                    <td>{hospitalizado.fecha_entrada}</td>
                    <td>{hospitalizado.fecha_salida}</td>
                    <td>{hospitalizado.diagnostico}</td>
                    
                   
                    <td>
                        <Link to={`/hospitalizados/editar/${hospitalizado._id}`} className= 'btn btn-success mt-2 mb-2 '><i class="fa-solid fa-pen-to-square"></i></Link>

                        


                        
                        
                        <button onClick={()=>deleteHospitalizados(hospitalizado._id)} className= 'btn btn-danger mt-2 mb-2'><i className="fa-solid fa-trash-can"></i></button>
                    </td>
                    
                </tr>


            ))}
        </tbody>




    </table>


    </div>
    </div>
    </div>


)


}
export default CompMostrarHospitalizado;