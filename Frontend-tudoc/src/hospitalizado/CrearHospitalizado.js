import axios from 'axios'
import { useState} from 'react'
import { useNavigate } from 'react-router-dom'

const URI='http://localhost:9000/api/hospitalizados/'

const CompCrearHospitalizados=()=>{
    const[nombres,setNombres]=useState('');
    const[apellidos,setApellidos]=useState('');
    const[numero_cama,setNumero_cama]=useState('');
    const[cedula,setCedula]=useState('');
    const[fecha_entrada,setFecha_entrada]=useState('');
    const[fecha_salida,setFecha_salida]=useState('');
    const[diagnostico,setDiagnostico]=useState('');
    const navigate= useNavigate();

    //funcion guardar
    const GuardarC= async(g) =>{
        g.preventDefault();
        await axios.post(URI,{ nombres:nombres, apellidos:apellidos,numero_cama:numero_cama,cedula:cedula,
        fecha_entrada:fecha_entrada,fecha_salida:fecha_salida,diagnostico:diagnostico});
        navigate('/');

    }
    return(
        <div>
        <h2>Modulo Guardar Hospitalizados</h2>
        <form onSubmit={GuardarC}>

        <div className='mb-3'>
        <label className='form-label'>Nombres</label>
        <input value={nombres} onChange={(g) =>setNombres(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Apellidos</label>
        <input value={apellidos} onChange={(g) =>setApellidos(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Numero_Cama</label>
        <input value={numero_cama} onChange={(g) =>setNumero_cama(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Cedula</label>
        <input value={cedula} onChange={(g) =>setCedula(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Fecha_Entrada</label>
        <input value={fecha_entrada} onChange={(g) =>setFecha_entrada(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Fecha_Salida</label>
        <input value={fecha_salida} onChange={(g) =>setFecha_salida(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Diagnostico</label>
        <input value={diagnostico} onChange={(g) =>setDiagnostico(g.target.value)}
        type="text" className='form-control' />
        </div>

        <button type='submit' className='btn btn-primary'>Guardar</button>





        </form>
        </div>
    )}
export default CompCrearHospitalizados;


