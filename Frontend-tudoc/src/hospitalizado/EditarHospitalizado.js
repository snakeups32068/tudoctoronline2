import axios from 'axios'
import { useState, useEffect} from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const URI='http://localhost:9000/api/hospitalizados/'

const CompEditarHospitalizados=()=>{
    const[nombres,setNombres]=useState('');
    const[apellidos,setApellidos]=useState('');
    const[numero_cama,setNumero_cama]=useState('');
    const[cedula,setCedula]=useState('');
    const[fecha_entrada,setFecha_Entrada]=useState('');
    const[fecha_salida,setFecha_salida]=useState('');
    const[diagnostico,setDiagnostico]=useState('');
    const navigate= useNavigate();
    const {id} = useParams();

//funcion actualizar

const Update= async(g) =>{
    g.preventDefault();
    await axios.put(`${URI}${id}`,{ nombres:nombres, apellidos:apellidos,numero_cama:numero_cama,
    cedula:cedula,fecha_entrada:fecha_entrada,fecha_salida:fecha_salida,diagnostico:diagnostico});
    navigate('/');
}
useEffect(()=>{
    gethospitalizadoById()
    // eslint-disable-next-line

},[])
const gethospitalizadoById= async()=>{
    const res= await axios.get(`${URI}${id}`)
    setNombres(res.data.nombres);
    setApellidos(res.data.apellidos);
    setNumero_cama(res.data.numero_cama);
    setCedula(res.data.cedula);
    setFecha_Entrada(res.data.fecha_entrada);
    setFecha_salida(res.data.fecha_salida);
    setDiagnostico(res.data.diagnostico);
    
}

return(
    <div>
        <h2>Modulo Editar Hopitalizados</h2>
        <form onSubmit={Update}>

        <div className='mb-3'>
        <label className='form-label'>Nombres</label>
        <input value={nombres} onChange={(g) =>setNombres(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Apellidos</label>
        <input value={apellidos} onChange={(g) =>setApellidos(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Numero_Cama</label>
        <input value={numero_cama} onChange={(g) =>setNumero_cama(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Cedula</label>
        <input value={cedula} onChange={(g) =>setCedula(g.target.value)}
        type="number" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Fecha_Entrada</label>
        <input value={fecha_entrada} onChange={(g) =>setFecha_Entrada(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'>Fecha_Salida</label>
        <input value={fecha_salida} onChange={(g) =>setFecha_salida(g.target.value)}
        type="text" className='form-control' />
        </div>

        <div className='mb-3'>
        <label className='form-label'> Diagnostico</label>
        <input value={diagnostico} onChange={(g) =>setDiagnostico(g.target.value)}
        type="text" className='form-control' />
        </div>
        <button type='submit' className='btn btn-primary'>Guardar</button>





        </form>
        </div>
    )






}
export default CompEditarHospitalizados;