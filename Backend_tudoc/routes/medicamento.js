//rutas medicamentos

const express=require('express');
const router= express.Router();
const medicamentoController=require('../controllers/medicamentoController');

//rutas crud

router.get('/',medicamentoController.buscarMedicamentos);
router.post('/',medicamentoController.agregarMedicamento);
router.get('/:id',medicamentoController.buscarMedicamento);
router.delete('/:id',medicamentoController.eliminarMedicamento);
router.put('/:id',medicamentoController.modificarMedicamento);


module.exports=router;