//rutas citas

const express=require('express');
const router= express.Router();
const hospitalizadoController=require('../controllers/hospitalizadoController');

//rutas crud

router.get('/',hospitalizadoController.buscarHospitalizados);
router.post('/',hospitalizadoController.agregarHospitalizado);
router.get('/:id',hospitalizadoController.buscarHospitalizado);
router.delete('/:id',hospitalizadoController.eliminarHospitalizado);
router.put('/:id',hospitalizadoController.modificarHospitalizado);


module.exports=router;