//rutas citas

const express=require('express');
const router= express.Router();
const pacienteController=require('../controllers/pacienteController');

//rutas crud

router.get('/',pacienteController.buscarPacientes);
router.post('/',pacienteController.agregarPaciente);
router.get('/:id',pacienteController.buscarPaciente);
router.delete('/:id',pacienteController.eliminarPaciente);
router.put('/:id',pacienteController.modificarPaciente);


module.exports=router;