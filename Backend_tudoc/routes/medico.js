//rutas citas

const express=require('express');
const router= express.Router();
const medicoController=require('../controllers/medicoController');

//rutas crud

router.get('/',medicoController.buscarMedicos);
router.post('/',medicoController.agregarMedico);
router.get('/:id',medicoController.buscarMedico);
router.delete('/:id',medicoController.eliminarMedico);
router.put('/:id',medicoController.modificarMedico);


module.exports=router;