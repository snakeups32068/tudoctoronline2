//rutas citas

const express=require('express');
const router= express.Router();
const citaController=require('../controllers/citaController');

//rutas crud

router.get('/',citaController.buscarCitas);
router.post('/',citaController.agregarCitas);
router.get('/:id',citaController.buscarCita);
router.delete('/:id',citaController.eliminarCita);
router.put('/:id',citaController.modificarCita);


module.exports=router;