const express = require('express');
const conectarDB =require('./config/db');
const cors=require ('cors');

//creamos nuestro servidor

const app = express();
const port = 9000;

//conectamos con la base de datos
conectarDB();
app.use(cors());
app.use(express.json());
//rutas
app.use('/api/citas/',require('./routes/cita'));
app.use('/api/hospitalizados',require('./routes/hospitalizado'));
app.use('/api/medicamentos',require('./routes/medicamento'));
app.use('/api/medicos',require('./routes/medico'));
app.use('/api/pacientes',require('./routes/paciente'));




//estamos conectados en la web
app.get("/",(req,res) =>{
    res.send("Binevenidos ya estamos conectados con el navegador");
    });

app.listen(port,() => {
    console.log("el servidor esta corriendo perfectamente",port);
});