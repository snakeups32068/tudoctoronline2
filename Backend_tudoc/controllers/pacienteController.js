const Paciente=require('../models/Paciente');
//buscar citas
exports.buscarPacientes=async(req,res)=> {
    try{
        const paciente =await Paciente.find();
        res.json(paciente);
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//agregar citas
exports.agregarPaciente= async(req,res)=>{
    try {
        let paciente;
        paciente=new Paciente(req.body);
        await paciente.save ();
        res.send(paciente);
        
        
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al cargar datos");
    }
}
//buscar por id
exports.buscarPaciente=async(req,res)=> {
    try{
        let paciente =await Paciente.findById(req.params.id);
        if (!paciente){
            res.status(404).json({msg: 'no existe el producto'});
        }
        res.json(paciente);
       
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//eliminar por id
exports.eliminarPaciente= async(req,res)=> {
    try{
        let paciente = await Paciente.findById(req.params.id);
        if (!paciente){
            res.status(404).json({msg: 'no existe la cita'});
        }
        await Paciente.findByIdAndRemove({_id:req.params.id});
        res.json({msg: 'registro eliminado'});
       
    }
    catch(error){
        console.log(error)
        res.status(500).send('hay un errror ');
    }
}

//modificar cita
exports.modificarPaciente= async(req,res) => {
    try{
        const {nombres,apellidos,cedula,telefono,email,fecha_nacimiento,tipo_sangre,direccion}=req.body;
        let paciente= await Paciente.findById(req.params.id);

        if(!paciente){
            res.status(404).json({msg: 'no existe la cita'});

        }
        paciente.nombres=nombres;
        paciente.apellidos=apellidos;
        paciente.cedula=cedula;
        paciente.telefono=telefono;
        paciente.email=email;
        paciente.fecha_nacimiento=fecha_nacimiento;
        paciente.tipo_sangre=tipo_sangre;
        paciente.direccion=direccion;
        paciente=await Paciente.findOneAndUpdate({_id:req.params.id},paciente,{new:true}); 
        res.json(paciente);
       }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror ")
    }
};