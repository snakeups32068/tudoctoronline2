const Cita=require('../models/Cita');
//buscar citas
exports.buscarCitas=async(req,res)=> {
    try{
        const citas =await Cita.find();
        res.json(citas);
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//agregar citas
exports.agregarCitas= async(req,res)=>{
    try {
        let cita;
        cita=new Cita(req.body);
        await cita.save ();
        res.send(cita);
        
        
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al cargar datos");
    }
}
//buscar por id
exports.buscarCita=async(req,res)=> {
    try{
        let cita =await Cita.findById(req.params.id);
        if (!cita){
            res.status(404).json({msg: 'no existe el producto'});
        }
        res.json(cita);
       
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//eliminar por id
exports.eliminarCita= async(req,res)=> {
    try{
        let cita = await Cita.findById(req.params.id);
        if (!cita){
            res.status(404).json({msg: 'no existe la cita'});
            return
        }
        await Cita.findByIdAndRemove({_id:req.params.id});
        res.json({msg: 'registro eliminado'});
       
    }
    catch(error){
        console.log(error)
        res.status(500).send('hay un errror ');
    }
}

//modificar cita
exports.modificarCita= async(req,res) => {
    try{
        const {nombres,apellidos,cedula,especialidad,doctor,fecha,hora,numero_consultorio}=req.body;
        let cita= await Cita.findById(req.params.id);

        if(!cita){
            res.status(404).json({msg: 'no existe la cita'});

        }
        cita.nombres=nombres;
        cita.apellidos=apellidos;
        cita.cedula=cedula;
        cita.especialidad=especialidad;
        cita.doctor=doctor;
        cita.fecha=fecha;
        cita.hora=hora;
        cita.numero_consultorio=numero_consultorio;
        cita=await Cita.findOneAndUpdate({_id:req.params.id},cita,{new:true}); 
        res.json(cita);
       }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror ")
    }
};