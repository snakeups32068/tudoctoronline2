const Medicamento=require('../models/Medicamento');
//buscar citas
exports.buscarMedicamentos=async(req,res)=> {
    try{
        const medicamento =await Medicamento.find();
        res.json(medicamento);
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//agregar citas
exports.agregarMedicamento= async(req,res)=>{
    try {
        let medicamento;
        medicamento=new Medicamento(req.body);
        await medicamento.save ();
        res.send(medicamento);
        
        
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al cargar datos");
    }
}
//buscar por id
exports.buscarMedicamento=async(req,res)=> {
    try{
        let medicamento =await Medicamento.findById(req.params.id);
        if (!medicamento){
            res.status(404).json({msg: 'no existe el producto'});
        }
        res.json(medicamento);
       
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//eliminar por id
exports.eliminarMedicamento= async(req,res)=> {
    try{
        let medicamento = await Medicamento.findById(req.params.id);
        if (!medicamento){
            res.status(404).json({msg: 'no existe la cita'});
        }
        await Medicamento.findByIdAndRemove({_id:req.params.id});
        res.json({msg: 'registro eliminado'});
       
    }
    catch(error){
        console.log(error)
        res.status(500).send('hay un errror ');
    }
}

//modificar cita
exports.modificarMedicamento= async(req,res) => {
    try{
        const {nombre_medicamento,fecha_vencimiento,cantidad,numero_registro,nombre_laboratorio}=req.body;
        let medicamento= await Medicamento.findById(req.params.id);

        if(!medicamento){
            res.status(404).json({msg: 'no existe la cita'});

        }
        medicamento.nombre_medicamento=nombre_medicamento;
        medicamento.fecha_vencimiento=fecha_vencimiento;
        medicamento.cantidad=cantidad;
        medicamento.numero_registro=numero_registro;
        medicamento.nombre_laboratorio=nombre_laboratorio;
        
        medicamento=await Medicamento.findOneAndUpdate({_id:req.params.id},medicamento,{new:true}); 
        res.json(medicamento);
       }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror ")
    }
};