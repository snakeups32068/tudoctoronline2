const Hospitalizado=require('../models/Hospitalizado');
//buscar citas
exports.buscarHospitalizados=async(req,res)=> {
    try{
        const hospitalizados =await Hospitalizado.find();
        res.json(hospitalizados);
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//agregar citas
exports.agregarHospitalizado= async(req,res)=>{
    try {
        let hospitalizado;
        hospitalizado=new Hospitalizado(req.body);
        await hospitalizado.save ();
        res.send(hospitalizado);
        
        
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al cargar datos");
    }
}
//buscar por id
exports.buscarHospitalizado=async(req,res)=> {
    try{
        let hospitalizado =await Hospitalizado.findById(req.params.id);
        if (!hospitalizado){
            res.status(404).json({msg: 'no existe el producto'});
        }
        res.json(hospitalizado);
       
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//eliminar por id
exports.eliminarHospitalizado= async(req,res)=> {
    try{
        let hospitalizado = await Hospitalizado.findById(req.params.id);
        if (!hospitalizado){
            res.status(404).json({msg: 'no existe la cita'});
        }
        await Hospitalizado.findByIdAndRemove({_id:req.params.id});
        res.json({msg: 'registro eliminado'});
       
    }
    catch(error){
        console.log(error)
        res.status(500).send('hay un errror ');
    }
}

//modificar cita
exports.modificarHospitalizado= async(req,res) => {
    try{
        const {nombres,apellidos,numero_cama,cedula,fecha_entrada,fecha_salida,diagnostico}=req.body;
        let hospitalizado= await Hospitalizado.findById(req.params.id);

        if(!hospitalizado){
            res.status(404).json({msg: 'no existe la cita'});

        }
        hospitalizado.nombres=nombres;
        hospitalizado.apellidos=apellidos;
        hospitalizado.numero_cama=numero_cama;
        hospitalizado.cedula=cedula;
        hospitalizado.fecha_entrada=fecha_entrada;
        hospitalizado.fecha_salida=fecha_salida;
        hospitalizado.diagnostico=diagnostico;
        hospitalizado=await Hospitalizado.findOneAndUpdate({_id:req.params.id},hospitalizado,{new:true}); 
        res.json(hospitalizado);
       }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror ")
    }
};