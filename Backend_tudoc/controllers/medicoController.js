const Medico=require('../models/Medico');
//buscar citas
exports.buscarMedicos=async(req,res)=> {
    try{
        const medicos =await Medico.find();
        res.json(medicos);
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//agregar citas
exports.agregarMedico= async(req,res)=>{
    try {
        let medico;
        medico=new Medico(req.body);
        await medico.save ();
        res.send(medico);
        
        
    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al cargar datos");
    }
}
//buscar por id
exports.buscarMedico=async(req,res)=> {
    try{
        let medico =await Medico.findById(req.params.id);
        if (!medico){
            res.status(404).json({msg: 'no existe el producto'});
        }
        res.json(medico);
       
    }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror al cargar los datos");
    }
}
//eliminar por id
exports.eliminarMedico= async(req,res)=> {
    try{
        let medico = await Medico.findById(req.params.id);
        if (!medico){
            res.status(404).json({msg: 'no existe la cita'});
            return
        }
        await Medico.findByIdAndRemove({_id:req.params.id});
        res.json({msg: 'registro eliminado'});
       
    }
    catch(error){
        console.log(error)
        res.status(500).send('hay un errror ');
    }
}

//modificar cita
exports.modificarMedico= async(req,res) => {
    try{
        const {nombres,apellidos,cedula,telefono,email,fecha_nacimiento,cargo,direccion,numero_consultorio}=req.body;
        let medico= await Medico.findById(req.params.id);

        if(!medico){
            res.status(404).json({msg: 'no existe la cita'});

        }
        medico.nombres=nombres;
        medico.apellidos=apellidos;
        medico.cedula=cedula;
        medico.telefono=telefono;
        medico.email=email;
        medico.fecha_nacimiento=fecha_nacimiento;
        medico.cargo=cargo;
        medico.direccion=direccion;
        medico.numero_consultorio=numero_consultorio;
        medico=await Medico.findOneAndUpdate({_id:req.params.id},medico,{new:true}); 
        res.json(medico);
       }
    catch(error){
        console.log(error)
        res.status(500).send("hay un errror ")
    }
};