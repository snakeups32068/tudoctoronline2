const mongoose = require('mongoose');

const medicamentoSchema = mongoose.Schema({
    
    
    nombre_medicamento:{
        type:String,
        required : true
    },
    fecha_vencimiento:{
        type:String,
        required : true
    },
    cantidad:{
        type:Number,
        required : true
    },
    numero_registro:{
        type:Number,
        required : true
    },
    nombre_laboratorio:{
        type:String,
        required : true
    }
},{ versionKey: false });


module.exports = mongoose.model('Medicamento',medicamentoSchema);