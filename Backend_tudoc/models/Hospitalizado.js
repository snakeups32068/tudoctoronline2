const mongoose = require('mongoose');

const hospitalizadoSchema = mongoose.Schema({
    
    
    nombres:{
        type:String,
        required : true
    },
    apellidos:{
        type:String,
        required : true
    },
    numero_cama:{
        type:Number,
        required : true
    },
    cedula:{
        type:Number,
        required : true
    },
    fecha_entrada:{
        type:String,
        required : true
    },
    fecha_salida:{
        type:String,
        required : true
    },
    diagnostico:{
        type:String,
        required : true
    }
},{ versionKey: false });


module.exports = mongoose.model('Hospitalizado',hospitalizadoSchema);