const mongoose = require('mongoose');

const citaSchema = mongoose.Schema({
    
    
    nombres:{
        type:String,
        required : true
    },
    apellidos:{
        type:String,
        required : true
    },
    cedula:{
        type:Number,
        required : true
    },
    especialidad:{
        type:String,
        required : true
    },
    doctor:{
        type:String,
        required : true
    },
    fecha:{
        type:String,
        required : true
    },
    hora:{
        type:String,
        required : true
    },
    numero_consultorio:{
        type:Number,
        required : true
    }
},{ versionKey: false });


module.exports = mongoose.model('Cita',citaSchema);