const mongoose = require('mongoose');

const medicoSchema = mongoose.Schema({
    
    
    nombres:{
        type:String,
        required : true
    },
    apellidos:{
        type:String,
        required : true
    },
    cedula:{
        type:Number,
        required : true
    },
    telefono:{
        type:Number,
        required : true
    },
    email:{
        type:String,
        required : true
    },
    fecha_nacimiento:{
        type:String,
        required : true
    },
    cargo:{
        type:String,
        required : true
    },
    direccion:{
        type:String,
        required : true
    },
    numero_consultorio:{
        type:Number,
        required : true
    }
},{ versionKey: false });


module.exports = mongoose.model('Medico',medicoSchema);